package ms.src.code.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class BufferedClass {
	
	public static void main(String[] args) throws IOException {
		/* 입출력 java.io 패키지가 있다.
		 * 그안에 바이트기반과, 문자기반으로 나뉘어져 있으며.
		 * 바이트기반의 최상위 클래스 InputStream / OutputStream으로 나뉘어져 있고,
		 * 문자기반의 최상위 클래스 Reader / Writer가 존재한다.
		 * 
		 * 입력 스트림 - InputStream 출력 스트림 - OutputStream
		 * 			   System.in			 Ststem.out
		 * 
		 * 그안에 보조스트림이 따로 존재한다. 보조 스트림은, 위의 InputStream & OutputStream을 보완하고자 사용된다.
		 * 오늘은 BufferdReader와 BufferedWriter를 공부한다.
		 * Reader와 Writer는 String 기반이다.
		 *
		 *  */
		
		// Buffer객체 생성시에는 InputStream과 InpustStreamReader의 합쳐진 형태를 취한다.
		// throws IDException 을 사용 하거나 try ~ catch문을 넣어주어야한다. 아니면 에러가난다.
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));		
		// BufferedReader의 객체 생성은 이렇게 해준다. Reader는 문자기반 
		System.out.print("문자열 입력 : ");

			String str = br.readLine();	// readLine은 기본값 String으로 리턴하기에 다른타입은 (형변환)해야한다
			System.out.println(str);
			
			int i = Integer.parseInt(br.readLine());	// Int
		
			

		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		// BufferedWriter의 객체 생성은 이렇게 해준다. Writer는 문자기반	
		
		bw.write(10 + "\n");
		bw.write('A');
		bw.write("안녕하세요");
		
		
		
		bw.flush();	
		bw.close();
		
		/* BufferedWriter의 출력 값은 write로 해주며, flush가 있어야 출력이 가능하다.
		 * \n는 개행문자로 '엔터'효과를 볼수 있다. 만약에 \ 를 출력을하고 싶다면
		 * \\를 두번해주게 된다면 \가 하나가 생긴다.
		 * */
		
		
		
		
		/* BufferedReader 많이 사용하는 메소드
		 * Close(); 입력 스트림을 닫고 사용하던 자원들을 푼다.
		 * read(); 한글자만 읽어 정수형으로 변환해준다
		 * readLine() String타입으로 한줄을 읽어온다.
		 * 
		 * 
		 * BufferedWriter 많이 사용하는 메소드 
		 * flush(); 버퍼에 남은 값 출력 && 버퍼 초기화(비운다.)
		 * Close(); 버퍼 끝내기 끝내기전에 반드시 flush 출력할것
		 * newLine(); 버퍼에서 사용하는 개행 메소드.
		 * write(int c) 한글자만 읽어 정수로 변환
		 * 
		 * */
		
	}

}

















